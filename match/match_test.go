package match

import (
	"1bet.fr/scraper/utils"
	"net/url"
	"strings"

	"testing"
)

func TestLeague_ListSources(t *testing.T) {
	t.Log("Testing matchendirect.fr sources...")
	scheduleUrl := "http://www.matchendirect.fr/france/ligue-1/"
	league := League{
		ScheduleUrl: &scheduleUrl,
	}
	sources, err := league.ListSources()
	if err != nil {
		t.Error(err)
	}
	if len(sources) == 0 {
		t.Errorf("no sources found")
	}
	for _, s := range sources {
		if !strings.HasPrefix(s.URL.String(), "http://www.matchendirect.fr/france/ligue-1/") {
			t.Errorf("unexpected source url %s", s.URL)
		}
	}

	t.Log("Testing eurosport.fr sources...")
	scheduleUrl = "https://www.eurosport.fr/tennis/open-d-australie-messieurs/2020/standing.shtml"
	league = League{
		ScheduleUrl: &scheduleUrl,
	}
	sources, err = league.ListSources()
	if err != nil {
		t.Error(err)
	}
	if len(sources) == 0 {
		t.Errorf("no sources found")
	}
	for _, s := range sources {
		if !strings.HasPrefix(s.URL.String(), "https://www.eurosport.fr/") {
			t.Errorf("unexpected source url %s", s.URL)
		}
	}

	t.Log("Testing rugbyrama.fr sources...")
	scheduleUrl = "https://www.rugbyrama.fr/rugby/top-14/calendar-result.shtml"
	league = League{
		ScheduleUrl: &scheduleUrl,
	}
	sources, err = league.ListSources()
	if err != nil {
		t.Error(err)
	}
	if len(sources) == 0 {
		t.Errorf("no sources found")
	}
	for _, s := range sources {
		if !strings.HasPrefix(s.URL.String(), "https://www.rugbyrama.fr/") {
			t.Errorf("unexpected source url %s", s.URL)
		}
	}
}

func TestSource_GetMatches(t *testing.T) {
	sourceUrl, _ := url.Parse("https://www.matchendirect.fr/france/ligue-1/2020-37/")
	league := &League{Id: 1, MatchDays: utils.IntPointer(38), MatchesByMatchDay: utils.IntPointer(10)}
	source := newSource(league, sourceUrl.Scheme, sourceUrl.Host, sourceUrl.Path, "", "", 0)
	matches, err := source.GetMatches()
	if err != nil {
		t.Error(err)
	}
	for _, m := range matches {
		if m.League == nil {
			t.Error("unexpected nil match.League")
		}
		if m.StartDate == nil {
			t.Error("unexpected nil match.StartDate")
		}
		if m.TeamHome.Names == nil {
			t.Error("unexpected nil match.TeamHome.Names")
		}
		if m.TeamAway.Names == nil {
			t.Error("unexpected nil match.TeamAway.Names")
		}
		if m.PlayerHome == nil {
			t.Error("unexpected nil match.PlayerHome")
		}
		if m.PlayerAway == nil {
			t.Error("unexpected nil match.PlayerAway")
		}
		if m.MatchDay == nil {
			t.Error("unexpected nil match.MatchDay")
		}
		if m.MatchDayId == nil {
			t.Error("unexpected nil match.MatchDayId")
		}
		if m.Error != nil {
			t.Errorf("unexpected not nil match.Error : %s", *m.Error)
		}
	}
}
