module 1bet.fr/scraper

go 1.12

require (
	github.com/PuerkitoBio/goquery v1.6.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/lib/pq v1.8.0
	github.com/mmcdole/gofeed v1.1.0
	golang.org/x/net v0.0.0-20201009032441-dbdefad45b89
)
