package postgres

import (
	"1bet.fr/scraper/match"
	"testing"
	"time"

	"1bet.fr/scraper/news"
	"1bet.fr/scraper/utils"
)

var (
	se *news.Source
	ns *news.News
	tm *match.Team
	mh *match.Match
)

func TestConnect(t *testing.T) {
	return
}

func TestListLeagues(t *testing.T) {
	leagues, err := ListLeagues()
	if err != nil {
		t.Errorf("unexpected error : %s", err)
	}
	if len(leagues) == 0 {
		t.Errorf("no league got from ListLeagues function")
	}
}

func TestUpdateLeague(t *testing.T) {
	league := &match.League{Id: 1}
	updated, err := UpdateLeague(league)
	if err != nil {
		t.Error(err)
	}
	if updated != 1 {
		t.Errorf("unexpected %d updated rows", updated)
	}
}

func TestListSources(t *testing.T) {
	sources, err := ListSources()
	se = sources[0]
	if err != nil {
		t.Errorf("unexpected error : %s", err)
	}
	if len(sources) == 0 {
		t.Errorf("no source got from ListSources function")
	}
}

func TestInsertNews(t *testing.T) {
	tags := []string{"Test", "Hello Toto"}
	cleanTags := []string{"test", "hello-toto"}
	nowTime := time.Now()
	ns = &news.News{
		Source: se,
		PubDate: &nowTime,
		Link: "https://test.com/toto",
		Title: "Hello toto",
		CleanTitle: "hello-toto",
		Tags: &tags,
		CleanTags: &cleanTags,
	}
	err := InsertNews(ns)
	if err != nil {
		t.Error(err)
	}
	if ns.Id == 0 {
		t.Errorf("unexpected value 0 for n.Id")
	}
}

func TestUpdateNews(t *testing.T) {
	content := []string{"toto", "test"}
	ns.Content = &content
	ns.Author = utils.StringPointer("T. Toto")
	updated, err := UpdateNews(ns)
	if err != nil {
		t.Error(err)
	}
	if updated != 1 {
		t.Errorf("unexpected %d updated rows", updated)
	}
}

func TestDeleteNews(t *testing.T) {
	deleted, err := DeleteNews(ns)
	if err != nil {
		t.Error(err)
	}
	if deleted != 1 {
		t.Errorf("unexpected %d news deleted", deleted)
	}
}

func TestInsertTeamBySourceName(t *testing.T) {
	teamNames := map[string]string{utils.HostMatchendirect: "Toto"}
	lg := &match.League{
		Id: 1,
		Sport: &match.Sport{Id: 1},
		Country: &match.Country{Id: 1},
		Gender: utils.IntPointer(match.GenderMale),
	}
	tm = match.NewTeam(utils.IntPointer(match.GenderMale), lg.Sport, lg.Country)
	tm.Names = &teamNames
	for range []int{0, 1} {
		if err := InsertTeamBySourceName(tm); err != nil {
			t.Errorf("unexpected error : %s", err)
		}
		if tm.Id == 0 {
			t.Error("unexpected zero team.Id")
		}
	}
}

func TestInsertMatch(t *testing.T) {
	startDate := time.Now()
	mh = match.NewMatch(&match.League{Id: 1}, tm, tm, &match.Player{}, &match.Player{}, "", 0, 0)
	mh.StartDate = &startDate
	mh.BaseUrl = utils.StringPointer("https://test.com/toto")
	for range []int{0, 1} {
		if err := InsertMatch(mh); err != nil {
			t.Error(err)
		}
		if mh.Id == 0 {
			t.Errorf("unexpected zero match.Id")
		}
	}
}

func TestDeleteMatch(t *testing.T) {
	deleted, err := DeleteMatch(mh)
	if err != nil {
		t.Error(err)
	}
	if deleted != 1 {
		t.Errorf("unexpected %d matches deleted", deleted)
	}
}

func TestDeleteTeam(t *testing.T) {
	deleted, err := DeleteTeam(tm)
	if err != nil {
		t.Error(err)
	}
	if deleted != 1 {
		t.Errorf("unexpected %d teams deleted", deleted)
	}
}

func TestClose(t *testing.T) {
	Close()
}
