package utils

import (
	"testing"
	"time"
)

func TestSanitize(t *testing.T) {
	if res := Sanitize("abcdef0123ABCDEFéçè_ ?.!=îôù"); res != "abcdef0123abcdefece------iou" {
		t.Errorf("unexpected Sanitize() answer '%s' != 'abcdef0123abcdefece------iou'", res)
	}
}

func TestArrayContains(t *testing.T) {
	if ArrayPointerContains(nil, "toto") {
		t.Errorf("unexpected contains true for nil array")
	}
	arr := []string{"bird", "apple", "ocean", "fork", "anchor"}
	if !ArrayPointerContains(&arr, "bird") {
		t.Errorf("unexpected contains false")
	}
	arr = []string{"bird", "apple", "ocean", "fork", "anchor"}
	if ArrayPointerContains(&arr, "potato") {
		t.Errorf("unexpected contains true")
	}
}

func TestAtoI(t *testing.T) {
	if res := AtoI("3"); res != 3 {
		t.Errorf("unexpected answer %d != 3", res)
	}
}

func TestEnglishDateString(t *testing.T) {
	if res := EnglishDateString("Mercredi 03 février 2021"); res != "Wednesday 03 February 2021" {
		t.Errorf("unexpected date format : %s", res)
	}
}

func TestStringPointer(t *testing.T) {
	if res := StringPointer(""); res != nil {
		t.Errorf("unexpected res : %s", *res)
	}
	if res := StringPointer("toto"); res == nil {
		t.Errorf("unexpected res : nil")
	} else {
		if *res != "toto" {
			t.Errorf("unexpected res : %s", *res)
		}
	}
}

func TestIntPointer(t *testing.T) {
	if res := IntPointer(0); res != nil {
		t.Errorf("unexpected res : %d", *res)
	}
	if res := IntPointer(123); res == nil {
		t.Errorf("unexpected res : nil")
	} else if *res != 123 {
		t.Errorf("unexpected res : %d", *res)
	}
}

func TestArrayPointerAppend(t *testing.T) {
	var arr *[]string
	if arr = ArrayPointerAppend(arr, "toto"); arr == nil {
		t.Errorf("unexpected arr : nil")
	} else if len(*arr) != 1 {
		t.Errorf("unexpected arr len : %d", len(*arr))
	} else if (*arr)[0] != "toto" {
		t.Errorf("unexpected arr content : %s", *arr)
	}

	if arr = ArrayPointerAppend(arr, "test"); arr == nil {
		t.Errorf("unexpected arr : nil")
	} else if len(*arr) != 2 {
		t.Errorf("unexpected arr len : %d", len(*arr))
	} else if (*arr)[1] != "test" {
		t.Errorf("unexpected arr content : %s", *arr)
	}
}

func TestArrayPointerJoin(t *testing.T) {
	if s := ArrayPointerJoin(nil, "-"); s != "" {
		t.Errorf("unexpected join result : %s", s)
	}
	arr := []string{"toto", "test"}
	if s:= ArrayPointerJoin(&arr, "-"); s != "toto-test" {
		t.Errorf("unexpected join result : %s", s)
	}
}

func TestFrenchTimezone(t *testing.T) {
	t1, _ := time.Parse("02 January 2006", "01 January 2020")
	if tz1 := FrenchTimezone(t1); tz1 != "CET" {
		t.Errorf("unexpected tz %s for date %s", tz1, t1)
	}
	t2, _ := time.Parse("02 January 2006", "28 March 2020")
	if tz2 := FrenchTimezone(t2); tz2 != "CET" {
		t.Errorf("unexpected tz %s for date %s", tz2, t2)
	}
	t3, _ := time.Parse("02 January 2006", "29 March 2020")
	if tz3 := FrenchTimezone(t3); tz3 != "CEST" {
		t.Errorf("unexpected tz %s for date %s", tz3, t3)
	}
	t4, _ := time.Parse("02 January 2006", "24 October 2020")
	if tz4 := FrenchTimezone(t4); tz4 != "CEST" {
		t.Errorf("unexpected tz %s for date %s", tz4, t4)
	}
	t5, _ := time.Parse("02 January 2006", "25 October 2020")
	if tz5 := FrenchTimezone(t5); tz5 != "CET" {
		t.Errorf("unexpected tz %s for date %s", tz5, t5)
	}
	t6, _ := time.Parse("02 January 2006", "31 December 2020")
	if tz6 := FrenchTimezone(t6); tz6 != "CET" {
		t.Errorf("unexpected tz %s for date %s", tz6, t6)
	}
}