package utils

import (
	"log"
	"strconv"
	"strings"
	"time"
)

const (
	HostMatchendirect = "www.matchendirect.fr"
	HostEurosport = "www.eurosport.fr"
	HostRugbyrama = "www.rugbyrama.fr"
	HostFFTT = "www.fftt.com"
	HostFootmercato = "www.footmercato.net"
	HostLequipe = "www.lequipe.fr"

	KeywordMatchendirect = "matchendirect"
	//KeywordEurosport = "eurosport"
	//KeywordRugbyrama = "rugbyrama"
	//KeywordFFTT = "fftt"
	//KeywordFootmercato = "footmercato"
	//KeywordLequipe = "lequipe"
)

func Sanitize(s string) (t string) {
	symbols := map[rune]string{
		'á': "a", 'Á': "a", 'à': "e", 'À': "a", 'â': "a", 'Â': "a", 'ä': "a", 'Ä': "a", 'ã': "a",
		'ç': "c",
		'é': "e", 'É': "e", 'è': "e", 'È': "e", 'ê': "e", 'Ê': "e", 'ë': "e", 'Ë': "e",
		'í': "i", 'Í': "i", 'ì': "i", 'Ì': "i", 'î': "i", 'Î': "i", 'ï': "i", 'Ï': "i",
		'ñ': "n",
		'ó': "o", 'Ó': "o", 'ò': "o", 'Ò': "o", 'ô': "o", 'Ô': "o", 'ö': "o", 'Ö': "o", 'ø': "o",
		'ú': "u", 'Ú': "u", 'ù': "u", 'Ù': "u", 'û': "u", 'Û': "u", 'ü': "u", 'Ü': "u",
	}

	for _, c := range s {
		if int(c) >= int('a') && int(c) <= int('z') {
			t += string(c)
		} else if int(c) >= int('0') && int(c) <= int('9') {
			t += string(c)
		} else if int(c) >= int('A') && int(c) <= int('Z') {
			t += string(rune(int(c) - int('A') + int('a')))
		} else if v, ok := symbols[c]; ok {
			t += v
		} else {
			t += "-"
		}
	}
	return t
}

func AtoI(s string) int {
	res, err := strconv.Atoi(s)
	if err != nil {
		log.Fatalf("error while converting '%s' to int : %s", s, err)
	}
	return res
}

func EnglishDateString(s string) string {
	months := map[string]string{
		"janvier": 		"January",
		"février": 		"February",
		"mars": 		"March",
		"avril": 		"April",
		"mai": 			"May",
		"juin": 		"June",
		"juillet": 		"July",
		"août": 		"August",
		"septembre": 	"September",
		"octobre":		"October",
		"novembre": 	"November",
		"décembre": 	"December",
	}
	days := map[string]string{
		"lundi": 		"Monday",
		"mardi": 		"Tuesday",
		"mercredi": 	"Wednesday",
		"jeudi": 		"Thursday",
		"vendredi": 	"Friday",
		"samedi": 		"Saturday",
		"dimanche": 	"Sunday",
	}

	s = strings.TrimSpace(strings.ToLower(s))
	for fr, en := range months {
		if strings.Contains(s, fr) {
			s = strings.ReplaceAll(s, fr, en)
			break
		}
	}
	for fr, en := range days {
		if strings.Contains(s, fr) {
			s = strings.ReplaceAll(s, fr, en)
			break
		}
	}
	return s
}

func StringPointer(s string) *string {
	if s == "" {
		return nil
	}
	s = strings.TrimSpace(s)
	return &s
}

func IntPointer(i int) *int {
	if i == 0 {
		return nil
	}
	return &i
}

func ArrayPointerContains(arr *[]string, val string) bool {
	if arr == nil {
		return false
	}
	for _, elt := range *arr {
		if elt == val {
			return true
		}
	}
	return false
}

func ArrayPointerAppend(a *[]string, v string) *[]string {
	if a == nil {
		r := []string{strings.TrimSpace(v)}
		return &r
	}
	r := append(*a, strings.TrimSpace(v))
	return &r
}

func ArrayPointerJoin(a *[]string, sep string) string {
	if a == nil {
		return ""
	}
	return strings.Join(*a, sep)
}

func FrenchTimezone(t time.Time) string {
	lastMarchSunday, _ := time.Parse("02 January 2006", "31 March " + strconv.FormatInt(int64(t.Year()), 10))
	for int(lastMarchSunday.Weekday()) > 0 {
		lastMarchSunday = lastMarchSunday.Add(-24 * time.Hour)
	}

	lastOctoberSunday, _ := time.Parse("02 January 2006", "31 October " + strconv.FormatInt(int64(t.Year()), 10))
	for int(lastOctoberSunday.Weekday()) > 0 {
		lastOctoberSunday = lastOctoberSunday.Add(-24 * time.Hour)
	}

	if (t.After(lastMarchSunday) || t.Equal(lastMarchSunday)) && t.Before(lastOctoberSunday) {
		return "CEST"
	}
	return "CET"
}
