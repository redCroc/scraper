package news

import (
	"strings"
	"testing"

	"1bet.fr/scraper/utils"
)

func TestNews_Feed(t *testing.T) {
	var n *News

	t.Logf("testing feed from Eurosport")
	n = &News{
		Source: &Source{Sport: &Sport{Name: "Football", CleanName: "football"}},
		Link: "https://www.eurosport.fr/football/bundesliga/2020-2021/dortmund-au-tapis-thuram-debloque-son-compteur_sto7905745/story.shtml",
	}
	if err := n.Feed(); err != nil {
		t.Errorf("unexpected error : %s", err)
	}
	if !strings.HasPrefix(*n.Teaser, "BUNDESLIGA – Le Borussia Dortmund et ses jeunes stars ont chuté") {
		t.Errorf("unexpected teaser : %s", *n.Teaser)
	}
	if !strings.HasPrefix((*n.Content)[0], "Etonnante Bundesliga. Dortmund battu, Leipzig tenu en échec samedi,") {
		t.Errorf("unexpected content : %s", (*n.Content)[0])
	}
	if *n.Author != "Eurosport" {
		t.Errorf("unexpected author : %s", *n.Author)
	}
	if !utils.ArrayPointerContains(n.CleanTags, "bundesliga") {
		t.Errorf("cleanTags does not contain bundesliga")
	}

	t.Logf("testing feed from L'équipe")
	n = &News{
		Source: &Source{Sport: &Sport{Name: "Football", CleanName: "football"}},
		Link: "https://www.lequipe.fr/Football/Actualites/Mitchel-bakker-psg-je-vais-devoir-elever-mon-niveau-de-jeu/1176182",
	}
	if err := n.Feed(); err != nil {
		t.Errorf("unexpected error : %s", err)
	}
	if !strings.HasPrefix(*n.Teaser, "Mitchel Bakker, le latéral néerlandais du PSG") {
		t.Errorf("unexpected teaser : %s", *n.Teaser)
	}
	if !strings.HasPrefix((*n.Content)[0], "« Les absences de Juan Bernat et Layvin Kurzawa") {
		t.Errorf("unexpected content : %s", (*n.Content)[0])
	}
	if *n.Author != "H. De." {
		t.Errorf("unexpected author : %s", *n.Author)
	}
	if !utils.ArrayPointerContains(n.CleanTags, "paris-sg--fra-") {
		t.Errorf("cleanTags does not contain paris-sg--fra-")
	}

	t.Logf("testing feed from FFTT")
	n = &News{
		Source: &Source{Sport: &Sport{Name: "Tennis de Table", CleanName: "tennis-de-table"}},
		Link: "http://www.fftt.com/site/actualites/2020-09-22/laura-gasnier-page-qui-se-tourne-avec-bleues",
	}
	if err := n.Feed(); err != nil {
		t.Errorf("unexpected error : %s", err)
	}
	if !strings.HasPrefix(*n.Teaser, "Après 15 années en équipe de France, Laura Gasnier a décidé") {
		t.Errorf("unexpected teaser : %s", *n.Teaser)
	}
	if !strings.HasPrefix((*n.Content)[0], "Elle évoque un choix personnel qui a demandé plusieurs mois de réflexion") {
		t.Errorf("unexpected content : %s", (*n.Content)[0])
	}
	if n.Author != nil {
		t.Errorf("unexpected author : %s", *n.Author)
	}
	if !utils.ArrayPointerContains(n.CleanTags, "gasnier-laura") {
		t.Errorf("cleanTags does not contain gasnier-laura")
	}

	t.Logf("testing feed from Foot Mercato")
	n = &News{
		Source: &Source{Sport: &Sport{Name: "Football", CleanName: "football"}},
		Link:    "https://www.footmercato.net/a3190892483125730002-real-madrid-personne-ne-veut-de-luka-jovic",
	}
	if err := n.Feed(); err != nil {
		t.Errorf("unexpected error : %s", err)
	}
	if !strings.HasPrefix(*n.Teaser, "Alors que la date de fin du mercato approche considérablement,") {
		t.Errorf("unexpected teaser : %s", *n.Teaser)
	}
	if !strings.HasPrefix((*n.Content)[0], "Tic-tac, tic-tac... Le chrono défile, et le Real Madrid") {
		t.Errorf("unexpected content : %s", (*n.Content)[0])
	}
	if *n.Author != "Max Franco Sanchez" {
		t.Errorf("unexpected author : %s", *n.Author)
	}
	if !utils.ArrayPointerContains(n.CleanTags, "football") {
		t.Errorf("cleanTags does not contain football")
	}

	t.Logf("testing feed from Rugbyrama")
	n = &News{
		Source: &Source{Sport: &Sport{Name: "Rugby", CleanName: "rugby"}},
		Link:    "https://www.rugbyrama.fr/rugby/top-14/2018-2019/top-14-face-au-racing-92-toulouse-n-aura-pas-de-marge-de-manoeuvre_sto7939622/story.shtml",
	}
	if err := n.Feed(); err != nil {
		t.Errorf("unexpected error : %s", err)
	}
	if !strings.HasPrefix(*n.Teaser, "TOP 14 - Opposé au Racing 92 à la Paris la Défense Arena") {
		t.Errorf("unexpected teaser : %s", *n.Teaser)
	}
	if !strings.HasPrefix((*n.Content)[0], "Réaliser et produire le même contenu") {
		t.Errorf("unexpected content : %s", (*n.Content)[0])
	}
	if *n.Author != "Rugbyrama" {
		t.Errorf("unexpected author : %s", *n.Author)
	}
	if !utils.ArrayPointerContains(n.CleanTags, "top-14") {
		t.Errorf("cleanTags does not contain football")
	}
}

func TestSource_ListNews(t *testing.T) {
	links := []string{
		"http://www.eurosport.fr/football/rss.xml",
		"https://www.lequipe.fr/rss/actu_rss_Football.xml",
		"http://www.fftt.com/site/medias/flux/rss_competition.xml",
		"http://www.footmercato.net/flux-rss",
	}

	for _, link := range links {
		t.Logf("testing ListNews from %s", link)
		source := &Source{FeedUrl: link}
		newsList, err := source.ListNews()
		if err != nil {
			t.Errorf("unexpected error : %s", err)
		}
		if len(newsList) == 0 {
			t.Errorf("no news parsed from rss")
		}
		for _, n := range newsList {
			if n.Title == "" {
				t.Errorf("unexpected empty news title")
			}
			if n.Link == "" {
				t.Errorf("unexpected empty news link")
			}
		}
	}
}
