package requests

import (
	"testing"
)

func TestGetDocumentFromURL(t *testing.T) {
	url := "https://check.torproject.org/"
	doc, err := GetDocumentFromURL(url)
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}
	h1 := doc.Find("h1")
	if h1.HasClass("off") {
		t.Errorf("tor is not correctly configured")
	}
}
